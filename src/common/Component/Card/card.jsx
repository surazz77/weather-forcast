import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    maxWidth: 400,
  },
});

export default function InfoCard(props) {
  const classes = useStyles();

  return (
    <Card className={classes.root} onClick={props.onClick}>
      <CardContent>
        <Typography variant="h5" component="h2">
          Date :{props.date}
          <br />
          Avg Temp:{props.avgTemp}
        </Typography>
      </CardContent>
    </Card>
  );
}
