import WeatherCollection from '../weatherCollection'

describe('WeatherCollection', () => {
  it('should create entity', () => {
    const weatherCollection = WeatherCollection.create({});

    expect(weatherCollection.data)
  })

  it('toFahrenheit should convert to correct fahrenheit temperature', () => {
    const weatherCollection = WeatherCollection.create({});

    const fahrenheitTemp = weatherCollection.toFahrenheit(276);

    expect(fahrenheitTemp).toBe(String(37.13))
  })
})